----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:29:26 03/10/2019 
-- Design Name: 
-- Module Name:    main - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
	port(
		clk_i : in std_logic;
		rst_i : in std_logic;
		led_o : out std_logic_vector(2 downto 0) := "000" 
		);
end main;
architecture Behavioral of main is
	constant default_led : std_logic_vector(2 downto 0) := "000";
	signal temp : std_logic_vector( 2 downto 0) := "000";
	signal temp_next : std_logic_vector( 2 downto 0) := "000";
begin
	process(clk_i, rst_i)
		begin
			if(rst_i = '1') then
				-- led_o <= default_led;
				-- temp <= default_led;
				-- temp_next <= default_led;
			elsif( clk_i'event  and clk_i='1' ) then
				temp <= std_logic_vector(unsigned(temp) + 1);
			end if;
		end process;
	process(temp)
		begin
			temp_next <= (std_logic_vector(shift_right(unsigned(temp), 1)) xor temp);
	end process;
			led_o <= temp_next;

end Behavioral;