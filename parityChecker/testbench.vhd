--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:48:33 03/10/2019
-- Design Name:   
-- Module Name:   /home/cheeky/xil/14.7/ISE_DS/test/testbench.vhd
-- Project Name:  test
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: main
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY testbench IS
END testbench;
 
ARCHITECTURE behavior OF testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT main
    PORT(
         ans : OUT  std_logic_vector(3 downto 0) := "1110";
         segs : OUT  std_logic_vector(7 downto 0);
         sws : IN  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal sws_i : std_logic_vector(7 downto 0) := "00000000";

 	--Outputs
   signal ans_i : std_logic_vector(3 downto 0) := "1110";
   signal segs_i : std_logic_vector(7 downto 0) := "00000000";
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: main PORT MAP (
          ans => ans_i,
          segs => segs_i,
          sws => sws_i
        );

sws_i <= "00000000", "00001111" after 10ns, "10000000" after 20ns;
END;
