----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:21:10 03/07/2019 
-- Design Name: 
-- Module Name:    main - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
	port(
		ans	: out std_logic_vector(3 downto 0) := "1110";
		segs	: out std_logic_vector(7 downto 0) := "00000000";
		sws	: in	std_logic_vector(7 downto 0)
		 );
end main;
architecture Behavioral of main is
	constant	odd	 		 :	std_logic_vector(7 downto 0) := "11111101";
	constant	even	 		 :	std_logic_vector(7 downto 0) := "11110011";
begin
	process(sws)
	begin
		ans <= "1110";
		
		if((sws(0) xor sws(1) xor sws(2) xor
			sws(3) xor sws(4) xor sws(5) xor
			sws(6) xor sws(7)) = '1') then
				segs <= odd;
		elsif((sws(0) xor sws(1) xor sws(2) xor
			sws(3) xor sws(4) xor sws(5) xor
			sws(6) xor sws(7)) = '0') then
				segs <= even;
		end if;
	end process;
end Behavioral;